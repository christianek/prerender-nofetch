<?php

namespace Nullsju\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('prerender:add')
            ->setDescription('Add a entry to the queue')
            ->addOption('url', 'u', InputOption::VALUE_REQUIRED, 'Url to page to be rendered');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $prerender = new \Nullsju\Prerender();
        $queue = $prerender->container->get('queue');
        $queue->add($input->getOption('url'));
    }
}