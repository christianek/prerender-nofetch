<?php

namespace Nullsju\Prerender;

use Nullsju\Exceptions\RenderErrorException;
use Nullsju\Exceptions\RenderLimitException;

class Render
{
    /**
     *  https://www.prerender.cloud
     *  https://07.no/webwiki/Passord
     */

    private $logger;

    /**
     * Api-keys can be found at https://www.prerender.cloud
     */
    private $apikey;

    /**
     *  Documentation for the prerender: https://www.prerender.cloud/documentation
     */
    private $url;

    private $frontends;

    public function __construct(Logger $logger, $url, $apikey, $frontends)
    {
        $this->logger = $logger->get('Render');
        $this->url = $url;
        $this->apikey = $apikey;
        $this->frontends = $frontends;
        $this->logger->info("New render with url: " . $url);
    }

    /**
     *  Only available function, does everything for you
     *  Send full url and basepath for where the static files should be saved
     */
    public function run($url)
    {
        $html = $this->render($url);

        if ($html && $path = $this->generatePath($url)) {
            return $this->save($path, $html);
        }

        return false;
    }

    /**
     *  Calls the prerender and returns the html if success
     */
    private function render($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->generateUrl($url));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            sprintf('X-Prerender-Token: %s', $this->apikey),
            'Prerender-Disable-Ajax-Preload: true',
            'Prerender-Disable-Ajax-Bypass: true'
        ]);

        $result = curl_exec($ch);

        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $error = curl_error($ch);

        if ($status === 429) { // 429 is the error kode for limit reached
            $this->logger->error("Prerender limit is reached for this hour");
            throw new RenderLimitException("Prerender limit exceeded", 1);
        } elseif ($error || $status !== 200) {
            $this->logger->error(sprintf("Could not render url, error: %s", curl_error($ch)));
            throw new RenderErrorException("Prerender error, check var/logs/Render.log", 1);
        } else {
            $this->logger->info("Rendering the url successful");
        }

        curl_close($ch);

        return ($result ? $result : false);
    }

    /**
     *  Make sure the url is safe and correct
     */
    private function generateUrl($url)
    {
        return sprintf("%s/%s", $this->url, trim($url, '/'));
    }

    /**
     *  Create the path for where to store the index.html file
     */
    private function generatePath($url)
    {
        $fragments = parse_url($url);
        $host = $fragments['host'];

        if (isset($this->frontends[$host]) && $frontend = $this->frontends[$host]) {
            $path = (isset($fragments['path']) ? trim($fragments['path'], '/') : '');
            $absolutePath = trim(sprintf("%s/%s", $frontend, $path), '/');

            return sprintf("/%s", $absolutePath);
        }
        return false;
    }

    /**
     *  Create the folder and save the file
     */
    private function save($path, $html)
    {
        if (!file_exists($path)) {
            try {
                mkdir($path, 0755, true);
            } catch (\Exception $e) {
                $this->logger->error("Could not create folder, not saving");
                return false;
            }
        }

        $filePath = sprintf("%s/index.html", $path);

        $saved = file_put_contents($filePath, $html);

        if ($saved) {
            $this->logger->info("Successfully saved rendered html to: " . $filePath);
        } else {
            $this->logger->error("Could not save rendered html to: " . $filePath);
        }

        return $saved;
    }
}
