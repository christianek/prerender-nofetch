<?php

namespace Nullsju\Prerender;

use Monolog\Handler\StreamHandler;
use Monolog\Logger as BaseLogger;

class Logger
{
    private $logDir;

    private $enabled;

    private $logHandlerKeys = ['Queue', 'Render', 'Process', 'Database'];

    private $logHandlers = [];

    public function __construct($rootDir, $enabled)
    {
        $this->logDir = sprintf("/%s/var/logs", trim($rootDir, '/'));
        $this->enabled = $enabled;

        if (!is_dir($this->logDir)) {
            if (!mkdir($this->logDir, 0755, true)) {
                throw new \Exception('Could not create log path, permission denied.');
            }
        }

        $this->createLogHandlers();
    }

    private function createLogHandlers()
    {
        foreach ($this->logHandlerKeys as $handler) {
            $logger = new BaseLogger($handler);
            $logPath = sprintf("%s/%s.log", $this->logDir, $handler);
            $stream = new StreamHandler($logPath, BaseLogger::DEBUG);
            $logger->pushHandler($stream);
            $this->logHandlers[$handler] = $logger;
        }
    }

    public function get($logger)
    {
        return isset($this->logHandlers[$logger]) ? $this->logHandlers[$logger] : null;
    }
}
